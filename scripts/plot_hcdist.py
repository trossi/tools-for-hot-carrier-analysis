import numpy as np
import matplotlib.pyplot as plt


def plot_hcdist(hcdist_fpath, out_fpath, do_show):
    npz = np.load(hcdist_fpath)
    t = -1
    time = npz['time_t'][t]
    dist_o = npz['dist_to'][t]
    dist_u = npz['dist_tu'][t]
    energy_o = npz['energy_o']
    energy_u = npz['energy_u']
    print(f'Plotting at time {time} as')

    plt.figure()
    plt.plot(energy_o, dist_o, color='b')
    plt.plot(energy_u, dist_u, color='r')
    plt.savefig(out_fpath)
    if do_show:
        plt.show()


if __name__ == '__main__':
    import argparse
    from argparse_util import ExistingPathType, FilePathType

    parser = argparse.ArgumentParser()
    parser.add_argument('hcdist_fpath', type=ExistingPathType)
    parser.add_argument('out_fpath', type=FilePathType)
    parser.add_argument('--show', action='store_true')
    args = parser.parse_args()

    plot_hcdist(args.hcdist_fpath, args.out_fpath, args.show)
