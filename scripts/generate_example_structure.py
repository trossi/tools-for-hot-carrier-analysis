from ase.io import write
from ase.cluster.octahedron import Octahedron

symbol = 'Ag'
cutoff = 2
length = 3 * cutoff + 1
atoms = Octahedron(symbol, length=length, cutoff=cutoff)
write(f'{symbol}{len(atoms)}.xyz', atoms)
