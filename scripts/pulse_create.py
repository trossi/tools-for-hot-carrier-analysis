if __name__ == '__main__':
    import argparse
    from argparse_util import FilePathType

    parser = argparse.ArgumentParser()
    parser.add_argument('pulse_fpath', type=FilePathType)
    parser.add_argument('frequency', type=float)
    args = parser.parse_args()

    from pulse import create_pulse, write_pulse
    pulse = create_pulse(args.frequency)
    write_pulse(pulse, args.pulse_fpath)
