import numpy as np


def atomdos(gpw_fpath, atomweight_fpath, aproj_i, out_fpath, energy_e):
    from gpaw.mpi import world
    from gpaw import GPAW
    from gpaw.lcaotddft.ksdecomposition import gauss_ij

    calc = GPAW(gpw_fpath, txt=None)
    eig_n = calc.get_eigenvalues()
    efermi = calc.get_fermi_level()

    # Read atomweights
    npz = np.load(atomweight_fpath)
    weight_am = npz['weight_am']
    d_m = npz['d_m']

    # Parse diagonal atomweight values
    weight_an = weight_am[:, d_m == 0]
    assert weight_an.shape[1] == len(eig_n)

    # Pick atoms
    weight_n = weight_an[aproj_i].sum(axis=0)

    # Check not-calculated (nan) elements of weight_n
    isnan_n = np.isnan(weight_n)
    nan_eig_n = eig_n[isnan_n]
    eig_n = eig_n[np.logical_not(isnan_n)]
    weight_n = weight_n[np.logical_not(isnan_n)]

    # DOS
    width = 0.07
    G_en = gauss_ij(energy_e, eig_n - efermi, width)
    projdos_e = np.dot(G_en, weight_n)

    # Set data to nan near not-calculated elements
    nan_G_en = gauss_ij(energy_e, nan_eig_n - efermi, width)
    nan_dos_e = np.sum(nan_G_en, axis=1)
    G_norm = 1.0 / (width * np.sqrt(2 * np.pi))
    nan_e = np.zeros_like(energy_e)
    nan_e[nan_dos_e > 0.001 * G_norm] = np.nan

    projdos_e += nan_e

    if world.rank == 0:
        with open(out_fpath, 'w') as f:
            f.write(f'# Projected density of states wrt Fermi level '
                    f'for atoms {aproj_i}\n')
            f.write(f'# Gaussian folding, width = {width:.4f} eV\n')
            np.savetxt(f, np.stack((energy_e, projdos_e)).T)


if __name__ == '__main__':
    import argparse
    from argparse_util import ExistingPathType, FilePathType

    parser = argparse.ArgumentParser()
    parser.add_argument('gpw_fpath', type=ExistingPathType)
    parser.add_argument('atomweight_fpath', type=ExistingPathType)
    parser.add_argument('out_fpath', type=FilePathType)
    parser.add_argument('--atoms', type=int, nargs='+')
    args = parser.parse_args()

    # Energy grid
    emin = -20.0
    emax = 20.0
    de = 0.01
    energy_e = np.arange(emin, emax + 0.5 * de, de)

    atomdos(args.gpw_fpath, args.atomweight_fpath, args.atoms,
            args.out_fpath, energy_e)
