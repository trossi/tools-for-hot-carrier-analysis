# Tools for hot-carrier analysis

## Version information

These analysis scripts are adapted and updated from
[the code](https://doi.org/10.5281/zenodo.3964229) published
with the article
[Hot-Carrier Generation in Plasmonic Nanoparticles: The Importance of Atomic Structure](https://doi.org/10.1021/acsnano.0c03004).

The scripts are tested with
Python 3.7.4,
GPAW 22.1.0,
ASE 3.22.1,
NumPy 1.17.2, and
SciPy 1.3.1.

## Example usage

This example demonstrates the calculation of hot-carrier distributions in an unrelaxed Ag201 nanoparticle.
The xyz file can be generated with `python scripts/generate_example_structure.py`.

### Preparations

The settings in the sbatch script `scripts/submit_example.sh` needs to be adapted
for the used HPC environment.
The steps below assume that `scripts/submit.sh` is a working sbatch script.

If the ground-state or time-propagation calculations do not finish
within the allocated time, the same script can be rerun to continue
the calculation. The `TIME_LIMIT` and `START_TIME` environment variables
defined in `scripts/submit.sh` are required for a clean exit of the calculation
within the allocated time.

The scripts require that the library functions in `src` are in PYTHONPATH:
`export PYTHONPATH=.../tools-for-hot-carrier-analysis/src:$PYTHONPATH`.

### Calculation workflow

These steps use `$DPATH` as the output data path. This can be set for example by
`export DPATH=data/Ag201-example`.

1. Ground state (`gs`):
    * Requires the structure file
    * `sbatch scripts/submit.sh scripts/gs.py Ag201.xyz $DPATH/gs/gs{.out,.gpw,-nonconv.gpw}`
    * Creates output files in `$DPATH/gs/`
2. Time propagation (`td`):
    * Requires `gs`
    * `sbatch scripts/submit.sh scripts/td.py $DPATH/{gs/gs.gpw,td-x/{td{.out,.gpw},dm.dat,fdm.ulm}}`
    * Creates output files in `$DPATH/td-x/`
3. Photoabsorption spectrum:
    * Requires `td`
    * `gpaw python scripts/tdspec.py $DPATH/td-x/{dm.dat,abs.dat}`
    * Creates output file in `$DPATH/td-x/abs.dat`
4. Unoccupied states (`unocc`):
    * Requires `gs`
    * `sbatch scripts/submit.sh scripts/unocc.py $DPATH/{gs/gs.gpw,unocc/{unocc{.out,.gpw},eig.dat,ksd{.out,.ulm}}}`
    * Creates output files in `$DPATH/unocc/`
5. Density of states:
    * Requires `unocc`
    * `gpaw python scripts/dos.py $DPATH/unocc/{unocc.gpw,dos.dat}`
    * Creates output file `$DPATH/unocc/dos.dat`
6. Spatial atomic weights (`atomweight`):
    * Requires `unocc`
    * `sbatch scripts/submit.sh scripts/atomweight.py $DPATH/unocc/{unocc.gpw,atomweight.npz} --eig_limits -6 6`
        * Use `--eig_limits` to give the range of included states (in eV)
    * Creates output file `$DPATH/unocc/atomweight.npz`
7. Atom-projected density of states:
    * Requires `unocc` and `atomweight`
    * `gpaw python scripts/atomdos.py $DPATH/unocc/{unocc.gpw,atomweight.npz,atomdos_4.dat} --atoms 4`
        * List atom indices (here for atom index 4)
    * Creates output file `$DPATH/unocc/atomdos_4.dat`
8. Kohn-Sham decomposition of frequency-space density matrix (`frho`):
    * Requires `td` and `unocc`
    * `sbatch scripts/submit.sh scripts/frho.py $DPATH/{unocc/{unocc.gpw,ksd.ulm},td-x/{fdm.ulm,frho}}`
    * Creates output files in `$DPATH/td-x/frho/`
9. Pulse preparation (`pulse`):
    * `gpaw python scripts/pulse_create.py $DPATH/td-x/pulse/pulse.json 3.75`
        * Pulse at 3.75 eV
    * Creates output file `$DPATH/td-x/pulse/pulse.json`
10. Pulse convolution of dipole moment:
    * Requires `td` and `pulse`
    * `gpaw python scripts/pulse_dm.py $DPATH/td-x/{dm.dat,pulse/{pulse.json,dm.dat}}`
    * Creates output file `$DPATH/td-x/pulse/dm.dat`
11. Pulse convolution of density matrix (`pulse_rho`):
    * Requires `frho` and `pulse`
    * `sbatch scripts/submit.sh scripts/pulse_rho.py $DPATH/{unocc/{unocc.gpw,ksd.ulm},td-x/{frho,pulse/{pulse.json,rho}}} --time 30e3`
        * Density matrix calculated at 30 fs
    * Creates output files in `$DPATH/td-x/pulse/rho/`
12. Hot-carrier distributions:
    * Requires `pulse_rho`
    * `gpaw python scripts/pulse_hcdist.py $DPATH/{unocc/{unocc.gpw,ksd.ulm},td-x/pulse/{pulse.json,rho,hcdist.npz}} --time 30e3`
    * Creates output file `$DPATH/td-x/pulse/hcdist.npz`
13. Atomic-scale hot-carrier distributions:
    * Requires `pulse_rho` and `atomweight`
    * `gpaw python scripts/pulse_hcatomdist.py $DPATH/{unocc/{unocc.gpw,ksd.ulm,atomweight.npz},td-x/pulse/{pulse.json,rho,hcatomdist_corners.npz}} --time 30e3 --atoms 4 10 18 22 28 49 65 80 83 91 94 110 130 145 146 150 152 167 182 191 193 195 199 200`
        * List atom indices (here for the corner atoms in example structure)
    * Creates output file `$DPATH/td-x/pulse/hcatomdist_corners.npz`
14. Atomic-scale hot-carrier amounts:
    * Requires `pulse_rho` and `atomweight`
    * `gpaw python scripts/pulse_hcatomamount.py $DPATH/{unocc/{unocc.gpw,ksd.ulm,atomweight.npz},td-x/pulse/{pulse.json,rho,hcatomamount.npz}} --time 30e3 --electrons -6 6 --holes -6 6`
        * Use `--electrons` and `--holes` to give ranges of included electron and hole states (in eV)
    * Creates output file `$DPATH/td-x/pulse/hcatomamount.npz`
15. Spatial hot-carrier density profile:
    * Requires `pulse_rho` and `atomweight` (for a list of degenerate states)
    * `gpaw python scripts/pulse_hcdns.py $DPATH/{unocc/{unocc.gpw,ksd.ulm,atomweight.npz},td-x/pulse/{pulse.json,rho,he1dns.cube}} electrons --time 30e3 --energy_limits 1 10`
        * Use `electrons` or `holes` to choose carriers and `--energy_limits` to give the range of included states (in eV)
    * Creates output file `$DPATH/td-x/pulse/he1dns.cube`

### Plotting examples

* Reading and plotting the npz files created by `pulse_hcdist.py` and `pulse_hcatomdist.py`:
    * `gpaw python scripts/plot_hcdist.py $DPATH/td-x/pulse/hcatomdist_corners.npz figs/hcatomdist_corners.pdf`
